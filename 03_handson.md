# Hands on

1. Clone the repository for Ruby on Rails framework at https://github.com/rails/rails
   1. Explore the version history by visualizing it as a graph.
   1. Who was the last person to modify README.md? (Hint: use git log with an argument)
   1. What is the oldest commit message and year? What tool was used before Git in this project?
   1. What was the commit message associated with the last modification to the Dalli GEM, line of Gemfile? (Hint: use git blame and git show)
   1. What happens when you do `git stash`? What do you see when running `git log --all --oneline`? Run `git stash pop` to undo what you did with git stash. In what scenario might this be useful?
   1. Git provides a configuration file (or dotfile) called `~/.gitconfig`. Create an alias in `~/.gitconfig` so that when you run `git graph`, you get the output of `git log --all --graph --decorate --oneline`. Information about git aliases can be found [here](https://git-scm.com/docs/git-config#Documentation/git-config.txt-alias).
   1. BASH provides a configuration file called `~/.bashrc`. Create aliases in `~/.bashrc` so that when you run `ggl`, you get the output of `git log --all --graph --decorate --oneline` and when you run `ggh`, you get the output of `git graph`.
